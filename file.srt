1
00:04:23,806 --> 00:04:26,558
<i>Good morning, James.</i>

2
00:04:26,558 --> 00:04:27,851
<i>How are you feeling?</i>

3
00:04:27,851 --> 00:04:29,102
Wait. What?

4
00:04:29,102 --> 00:04:32,313
<i>It's perfectly normal to feel confused.</i>

5
00:04:32,313 --> 00:04:35,900
<i>You just spent 120 years
in suspended animation.</i>

6
00:04:35,900 --> 00:04:37,527
What?

7
00:04:37,527 --> 00:04:39,320
<i>It's okay, James.</i>

8
00:04:39,320 --> 00:04:41,865
<i>-It's Jim.
-Jim.</i>

9
00:04:41,865 --> 00:04:45,660
<i>Just breathe. Everything is okay.</i>

10
00:04:45,660 --> 00:04:47,078
Where am I?

11
00:04:47,078 --> 00:04:49,247
<i>You're a passenger on the Starship Avalon,</i>

12
00:04:49,247 --> 00:04:52,959
<i>the Homestead Company's
premier interstellar starliner.</i>

13
00:04:53,336 --> 00:04:57,672
<i>We've nearly completed the voyage
from Earth to your new home.</i>

14
00:04:57,672 --> 00:05:00,133
<i>The colony world of Homestead II.</i>

15
00:05:00,384 --> 00:05:03,970
<i>A new world. A fresh start.</i>

16
00:05:03,970 --> 00:05:05,555
<i>Room to grow.</i>

17
00:05:05,555 --> 00:05:07,349
Yeah.

18
00:05:07,349 --> 00:05:09,434
<i>The Avalon is on final approach.</i>

19
00:05:09,434 --> 00:05:11,603
<i>For the next four months,
you'll enjoy space travel</i>

20
00:05:11,603 --> 00:05:13,563
<i>at its most luxurious.</i>

21
00:05:13,563 --> 00:05:16,566
<i>Food. Fun. Friends.</i>

22
00:05:16,566 --> 00:05:19,069
<i>-My friends.
-That's right, Jim.</i>

23
00:05:19,069 --> 00:05:22,530
<i>The ID band on your wrist is your key
to the wonders of the Avalon.</i>

