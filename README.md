### srt to ssa\/ass
converting a file from the .srt format to the .ass format
with the translation of content.\
require installed `translate-shell`
```
./to.sh file.srt
```
First the program indexes all entries in the `.srt` file.\
The resulting indices (beginning with 1) are compared\
with the numbers of the records themselves. At any breakpoint,\
you can exit the program and the program guided by the presence of `.text`\
and `.trans` files will start from the same place as it was interrupted.\
After the first breakpoint, the sequence of records from the `.srt` file\
will be taken as a sample(not indexes). Next, the program compares\
the `.text` file and then `.trans` with the sample in the same way.\
Each record is reorganized in one line with the record number.\
In the final `.ass` file all record numbers are deleted.

Version **v2.0.0** adds support for multi-line subtitles\
In `awk` the `gensub` function has been removed for better compatibility

Version **v3.0.0** replaced `translate-shell` with `wget` and "agent Mozilla"
