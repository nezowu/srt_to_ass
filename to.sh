#!/bin/bash
set -- en fr fr "${1%.*}" #[source lang] [target lang] [top screen lang] [file.srt]
echo parameters selected: $1[source] $2[target] $3[top screen]
ps -C tor &>/dev/null || { echo tor not running; exit; }

if ! [ -f "$4.srt" ]; then
	[ "$4" ] && echo wrong file name ||
		echo In second line of script set language parameters and start: program file
	exit
fi

chunk=100	#lines in chunk
#delay=1	#Delay between translation chunks, if torsocks is used - comment out
stamp=$(grep -x '[1-9][0-9]*' $4.srt)


error() {
	echo -n file $3 is $'\e[1;32m'
	if [[ "$1" != "$2" ]]; then
		echo $'\e[0m'messed, possible problems in these records:
		comm --nocheck-order -3 <(echo "$1") <(echo "$2")
		echo -n $'continue/stop[Enter/Any]: \e[33m'
		read -sn1
		if [[ "$REPLY" != "" ]]; then
			echo $'exit\e[0m'
			exit
		fi
	fi
	echo $'Ok...\e[0m'
}

translate() {
	(( n++ % 2 )) && . torsocks on || . torsocks off &>/dev/null
	last=$(wc -l <$3.text)
	error "$(echo "$stamp")" "$(grep -oP '^[1-9][0-9]*(?=-0 )' $3.text)" .text
	echo $'\e[1;32mTranslating...\e[0m'
	while read -rd$'\r'; do
		echo -ne "\e[10D$(((count+=chunk)<last?count:last))/$last" >/dev/stderr
		wget -U "Mozilla/5.0" -q -O- "http://translate.googleapis.com/translate_a/single?client=gtx&sl=$1&tl=$2&dt=t&q=$REPLY"
		sleep $((delay))
	done < <(sed "0~$chunk"'s/.*/&\r/; $s/[^\r]$/&\r/' $3.text) |
		grep -oP '^[,[]+"\K(\\"|[^"])+?(?=(\\n)?",")' |
		awk '/^[1-9][0-9]*-0/ {if(NR > 1) print ""; sub(/-0/, ""); printf $0; next}
			{printf "\\N%s", $0} END {print ""}' >$3.trans
	echo
	finish $3
}

finish() {
	error "$stamp" "$(grep -o '^[1-9][0-9]*' $1.trans)" .trans
	awk '
	NR==FNR {a=$1; sub(/^[1-9][0-9]* /, ""); A[a]=$0; next}
	/,,$/ && ($1 in A)      {$0=$0 A[$1]}
	{sub(/^[1-9][0-9]* /, "")}
	1' $1.trans $1.ass >$1.ASS
	rm $1.ass
	mv $1.{ASS,ass}
	dir=$(mktemp -d ${1:0:3}-ass.XXX)
	mv $1.{ass,trans,text} $dir
	echo all files are moved to the $dir directory
	echo $'\e[1;32mdone!\e[0m'
	exit
}

error "$(seq ${stamp##*$'\n'})" "$(echo "$stamp")" .srt

if grep -sqm1 '^[1-9][0-9]* ' $4.ass; then
	echo "You wish to continue?[Enter/Any]: "
	read -sn1
	if [[ "$REPLY" != "" ]]; then
		echo exit
		exit
	fi
	echo $'\e[1;32mContinue...\e[0m'
	if [ -f $4.trans ]; then
		finish $4
	elif [ -f "$4.text" ]; then
		translate $1 $2 $4
	else
		echo $'\e[33m.trans .text no such files\e[0m'
		exit
	fi
fi

#create .ass file without translation
awk -vsl="$1" -vtl="$2" -vft="$3" -vmar=",,0000,0000,0000,," -vf="$4.text" '
BEGIN		{FS="[0-9] --> 0"; print "[Script Info]\n\
ScriptType: v4.00+\n\
Collisions: Normal\n\
PlayDepth: 0\nTimer: 100,0000\n\
Video Aspect Ratio: 0\n\
WrapStyle: 0\n\
ScaledBorderAndShadow: no\n\
\n\
[V4+ Styles]\n\
Format: Name,Fontname,Fontsize,PrimaryColour,SecondaryColour,OutlineColour,BackColour,Bold,Italic,Underline,StrikeOut,ScaleX,ScaleY,Spacing,Angle,BorderStyle,Outline,Shadow,Alignment,MarginL,MarginR,MarginV,Encoding\n\
Style: "ft",Arial,10,&H00F9FFFF,&H00FFFFFF,&H00000000,&H00000000,-1,0,0,0,100,100,0,0,1,1,0,8,10,10,10,0\n\
Style: "(ft==sl?tl:sl)",Arial,18,&H00F9FFF9,&H00FFFFFF,&H00000000,&H00000000,-1,0,0,0,100,100,0,0,1,2,0,2,10,10,10,0\n\
\n\
[Events]\n\
Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text"
		}
/^[1-9][0-9]*$/	{num = $0; getline
		gsub(/,/, "."); gsub(/^.|.$/, "")
		time = "0,"$1","$2","; getline}
!/^\s*$/        {gsub(/<[^>]*>/, "")
		text = text (text?RS:"") $0
		line = line (line?"\\N":"") $0}
/^\s*$/         {print num, "Dialogue:", time (ft==sl?sl mar line:tl mar)
		 print num, "Dialogue:", time (ft==sl?tl mar:sl mar line)
		 print num "-0", text > f; text = line = ""}
' $4.srt > $4.ass

translate $1 $2 $4
